# Installing the compiler

## Required Packages

### Graphviz
The Graphviz requirement is misleading. It has "libgvc" in brackets but you really need to install "libgraphviz-dev".

## Vala not finding libvala shared library
You need to run this command to update the system's knowledge of the shared libraries installed with this command:
`sudo ldconfig /usr/local/lib`

After running the command above, Vala will "Just work".

## Need to rebuild from an older vala version
1. Clean the whole repo with `git clean -dfx`
2. Use the bootstrap commands but set `VALAC` to an older `valac` version. (Very important that the program name starts with `valac`).
3. You'll have a working version of the compiler in development again.