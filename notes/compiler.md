# Compiler

## Where was sources defined?
Seems to appear out of nowhere? Then it suddenly gets checked? wHat? 

## Be careful when debugging
Vala code is compiled to C. This also applies to the compiler itself too. Code may not run in the same order that it was originally written in Vala. 

Use a debugger to see what's going when the compiler is at work.

## Include debugging symbols
When compiling the compiler, append the `--enable-debug` flag to `./autogen.sh` like this:
`./autogen.sh --enable-debug`. 

An alternative method would be to use the `--enable-coverage` flag instead.

This also enables line numbers when generating stacktraces with gdb.

# Enable backtraces for debugging
You can also enable backtraces when debugging with gdb like this:
`G_DEBUG=fatal-criticals gdb --args my_valac_version my_test_program.vala`

When a CRITICAL occus, you'll be able to use `bt` or `bt full` in gdb to retreive a backtrace.
