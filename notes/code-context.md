# Code Context

## What does CodeContext even do?
All the default values, syntax, analyzers, attributes, pretty much everything related to understanding the code lives here.

## Adding default defines
`add_default_defines()`

Define strings are generated from the Vala version and a hard coded glib version.

## Adding default attributes
The default attributes that the compiler recognises are added by constructing a new `UsedAttr` object.

## Set target profile
`set_target_profile` method is controls how the code is generated. By default, it will add the "GOBJECT" definition and the add the glib and gobject packages.

## Add source filename method also adds default using statements
This method seems does way more then its name suggests. It actually is responsible for adding Vala, Genie, C source files and C Header files to the compiler's code context.

For Vala and Genie files, it even adds **default using statements.**

In my opinion, the `add_source_filename` should be refactored or its name should be updated to reflect all the tasks its actually doing
