# Uninstalling the compiler

If you want to go back to using the vala version from your operating system's package manager by default, perform this command in the repository's root directory
```sh
sudo make uninstall
```

