# Analyzer

## What's happening?

At this point, the compiler has read all the code's tokens however now it's in `valaesemanticanalyzer.vala` and it figures out the meaning behind each of those tokens - how are they relevant to the compiler's operations?