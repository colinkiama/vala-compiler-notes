# Easy Prerequisites Installation

## elementary OS

### First, check for updates

```bash
sudo apt update

```

### Then install the required prerequisites

```bash
sudo apt install git \
gcc \
libbison-dev \
libglib2.0-dev \
flex \
libbison-dev \
libgraphviz-dev \
make \
autoconf \
autoconf-archive \
automake \
libtool \
help2man \
weasyprint
